package com.formacionspring.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ejemplo2SpringbApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ejemplo2SpringbApplication.class, args);
	}

}
